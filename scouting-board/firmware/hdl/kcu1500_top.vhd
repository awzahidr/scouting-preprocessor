--
-- Top-level entity for KCU1500 dev kit
--
-- D. R. May 2018
library ieee;
use ieee.std_logic_1164.all;

use IEEE.NUMERIC_STD.all;

library unisim;
use unisim.vcomponents.all;

use work.top_decl.all;
use work.datatypes.all;

entity top is
    port (
        -- MGT ref clock
        mgtrefclk0_x0y3_p : in std_logic;
        mgtrefclk0_x0y3_n : in std_logic;

        -- Serial data ports for transceiver channel 0
        ch0_gthrxn_in : in std_logic;
        ch0_gthrxp_in : in std_logic;
        ch0_gthtxn_out : out std_logic;
        ch0_gthtxp_out : out std_logic;
      
        -- Serial data ports for transceiver channel 1
        ch1_gthrxn_in : in std_logic;
        ch1_gthrxp_in : in std_logic;
        ch1_gthtxn_out : out std_logic;
        ch1_gthtxp_out : out std_logic;
      
        -- Serial data ports for transceiver channel 2
        ch2_gthrxn_in : in std_logic;
        ch2_gthrxp_in : in std_logic;
        ch2_gthtxn_out : out std_logic;
        ch2_gthtxp_out : out std_logic;
      
        -- Serial data ports for transceiver channel 3
        ch3_gthrxn_in : in std_logic;
        ch3_gthrxp_in : in std_logic;
        ch3_gthtxn_out : out std_logic;
        ch3_gthtxp_out : out std_logic;
      
        -- Serial data ports for transceiver channel 4
        ch4_gthrxn_in : in std_logic;
        ch4_gthrxp_in : in std_logic;
        ch4_gthtxn_out : out std_logic;
        ch4_gthtxp_out : out std_logic;
      
        -- Serial data ports for transceiver channel 5
        ch5_gthrxn_in : in std_logic;
        ch5_gthrxp_in : in std_logic;
        ch5_gthtxn_out : out std_logic;
        ch5_gthtxp_out : out std_logic;
      
        -- Serial data ports for transceiver channel 6
        ch6_gthrxn_in : in std_logic;
        ch6_gthrxp_in : in std_logic;
        ch6_gthtxn_out : out std_logic;
        ch6_gthtxp_out : out std_logic;
      
        -- Serial data ports for transceiver channel 7
        ch7_gthrxn_in : in std_logic;
        ch7_gthrxp_in : in std_logic;
        ch7_gthtxn_out : out std_logic;
        ch7_gthtxp_out : out std_logic;
      
        -- User-provided ports for reset helper block(s)
        sysclk300_p : in std_logic;
        sysclk300_n : in std_logic;
--        hb_gtwiz_reset_all_in : in std_logic;

        -- Clock for PCIe
        sysclk_in_p  : in  std_logic;
        sysclk_in_n  : in  std_logic;
        sys_rst_n    : in  std_logic;

        pci_exp_rxp  : in  std_logic_vector(7 downto 0);
        pci_exp_rxn  : in  std_logic_vector(7 downto 0);
        pci_exp_txp  : out std_logic_vector(7 downto 0);
        pci_exp_txn  : out std_logic_vector(7 downto 0);
        
        sda                 : INOUT  STD_LOGIC;
        scl                 : INOUT  STD_LOGIC;
        I2C_MAIN_RESET_B_LS : out  STD_LOGIC
        );
end top;

architecture Behavioral of top is

    signal mgtrefclk0_x0y3_int : std_logic;
    signal sysclk300_in        : std_logic;


    signal d_align, d_algo, d_package : ldata(3 + (N_REGION - 1) * 4 downto 0);

    signal clk, rst : std_logic;

    signal axi_clk, axi_clk4, axi_rstn, axi_rst                  : std_logic;
    signal pcie_lnk_up                                           : std_logic                      := '0';
    signal m_axis_c2h_tdata_0, m_axis_h2c_tdata_0, test_tdata    : std_logic_vector(255 downto 0);  -- Not sure about test_tdata
    signal m_axis_c2h_tlast_0, m_axis_h2c_tlast_0, test_tlast    : std_logic;
    signal m_axis_c2h_tvalid_0, m_axis_h2c_tvalid_0, test_tvalid : std_logic;
    signal m_axis_c2h_tready_0, m_axis_h2c_tready_0, test_tready : std_logic;
    signal m_axis_c2h_tkeep_0, m_axis_h2c_tkeep_0, test_tkeep    : std_logic_vector(31 downto 0);
    signal packet_length                                         : std_logic_vector(15 downto 0)  := x"0020";
    signal fifo_cnt                                              : std_logic_vector(14 downto 0)  := (others => '0');
    signal fifo_read, fifo_write, fifo_empty                     : std_logic                      := '0';
    type state_s is (idle, wait_s1, wait_s2, wait_s3, wait_s4, read);
    signal state                                                 : state_s                        := idle;

    signal validVec, doneVec : std_logic_vector(7 downto 0);

--    signal link_status_out                            : std_logic_vector(0 downto 0);
--    signal link_down_latched_out                      : std_logic_vector(0 downto 0);
    signal init_done_int                              : std_logic_vector(0 downto 0);
    signal init_retry_ctr_int                         : std_logic_vector(3 downto 0);
    signal gtpowergood_vio_sync                       : std_logic_vector(7 downto 0);
    signal txpmaresetdone_vio_sync                    : std_logic_vector(7 downto 0);
    signal rxpmaresetdone_vio_sync                    : std_logic_vector(7 downto 0);
    signal gtwiz_reset_tx_done_vio_sync               : std_logic_vector(0 downto 0);
    signal gtwiz_reset_rx_done_vio_sync               : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_all_vio_int                 : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_pll_and_datapath_int    : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_datapath_int            : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_pll_and_datapath_vio_int : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_datapath_vio_int         : std_logic_vector(0 downto 0);
    signal link_down_latched_reset_vio_int            : std_logic_vector(0 downto 0);
    signal loopback                                   : std_logic_vector(23 downto 0);

    signal rx_data : std_logic_vector(8 * 32 - 1 downto 0);
    signal trigger_ila : std_logic;

    signal ocounter : std_logic_vector(31 downto 0);
    signal bcounter : std_logic_vector(31 downto 0);
    signal evt_word : std_logic_vector(31 downto 0);
    signal sRxbyteisaligned : std_logic_vector(7 downto 0);
    signal cdr_stable : std_logic_vector(0 downto 0);
    
    signal clk_i2c, hb_gtwiz_reset_clk_freerun_in, hb_gtwiz_reset_clk_freerun_int, clk_fb : std_logic;

   signal str_rd, str_wr, rst_i2c : std_logic;
   signal data_rd                 : std_logic_vector(7 downto 0);
   signal sCommaDet               : std_logic_vector(7 downto 0);
   signal link_delays             : delay_vector(7 downto 0);
   signal measured_delays         : delay_vector(7 downto 0);
   signal aligned, aligning       : std_logic;

    signal bc0 : std_logic;

    component fifo_generator_0 is
    PORT (
           s_aresetn                 : IN  std_logic := '0';
           m_axis_tvalid             : OUT std_logic := '0';
           m_axis_tready             : IN  std_logic := '0';
           m_axis_tdata              : OUT std_logic_vector(256-1 DOWNTO 0) := (OTHERS => '0');
           m_axis_tkeep              : OUT std_logic_vector(32-1 DOWNTO 0) := (OTHERS => '0');
           m_axis_tlast              : OUT std_logic := '0';
           s_axis_tvalid             : IN  std_logic := '0';
           s_axis_tready             : OUT std_logic := '0';
           s_axis_tdata              : IN  std_logic_vector(256-1 DOWNTO 0) := (OTHERS => '0');
           s_axis_tkeep              : IN  std_logic_vector(32-1 DOWNTO 0) := (OTHERS => '0');
           s_axis_tlast              : IN  std_logic := '0';
           m_aclk                    : IN  std_logic := '0';
           s_aclk                    : IN  std_logic := '0');
    end component;

    COMPONENT link_vio
      PORT (
        clk : IN STD_LOGIC;
        probe_in0   : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in1   : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in2   : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in3   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        probe_in4   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        probe_in5   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        probe_in6   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        probe_in7   : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in8   : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in9   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        probe_in10  : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in11  : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        probe_in12  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in13  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in14  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in15  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in16  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in17  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in18  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in19  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in20  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in21  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in22  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in23  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in24  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in25  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in26  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_in27  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        probe_out0  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out1  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out2  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out3  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out4  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out5  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out6  : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
        probe_out7  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out8  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out9  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out10 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
      );
    END COMPONENT;
    
    COMPONENT axi_ila_0
    PORT (
        clk : IN STD_LOGIC;
    
    
        trig_in : IN STD_LOGIC;
        trig_in_ack : OUT STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(255 DOWNTO 0); 
        probe2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe6 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    );
    END COMPONENT;
begin

  -- Set MGT clock via I2C 
  ---------------------------------------------------------------------------
  i2c_i : entity work.i2c_driver PORT MAP (
      clk => clk_i2c,  -- 50 MHz from PLL below.
      reset => rst_i2c,  -- TODO: Get this and the below signals from VIO
      str_wr => str_wr,
      str_rd => str_rd,
      data_rd => data_rd,
      sda => sda,
      scl => scl
    );

  I2C_MAIN_RESET_B_LS <= '1'; -- TODO: Set via VIO?
  ---------------------------------------------------------------------------


  -- Generate alignment signal (i.e. the BC0)
  ---------------------------------------------------------------------------
  gen_bc0 : entity work.bc0_generator
    port map (
        rst => rst,
        clk => clk,
        bc0 => bc0
    );
  ---------------------------------------------------------------------------


  -- Differential reference clock buffer for freerunning clock
  IBUFDS_FREERUN_INST : IBUFDS
  port map (
      I     => sysclk300_p,
      IB    => sysclk300_n,
      O     => sysclk300_in
  );

  PLLE3_ADV_INST : PLLE3_ADV
    generic map (
        COMPENSATION   => "AUTO",
        STARTUP_WAIT   => "FALSE",
        DIVCLK_DIVIDE  => 1,
        CLKFBOUT_MULT  => 2,
        CLKFBOUT_PHASE => 0.000,
        CLKOUT0_DIVIDE => 4,
        CLKOUT0_PHASE  => 0.000,
        CLKOUT0_DUTY_CYCLE => 0.500,
        CLKOUT1_DIVIDE => 12,
        CLKOUT1_PHASE  => 0.000,
        CLKOUT1_DUTY_CYCLE => 0.500,
        CLKIN_PERIOD       => 3.333)
     port map (
        CLKFBOUT => clk_fb,
        CLKOUT0  => hb_gtwiz_reset_clk_freerun_in,
        CLKOUT0B => open,
        CLKOUT1  => clk_i2c,
        CLKOUT1B => open,
        CLKFBIN  => clk_fb,
        CLKIN    => sysclk300_in,
        DADDR    => (others => '0'),
        DCLK     => '0',
        DEN      => '0',
        DI       => (others => '0'),
        DO       => open,
        DRDY     => open,
        DWE      => '0',
        CLKOUTPHYEN => '0',
        PWRDWN      => '0',
        RST      => '0'
     );
     
--  BUFG_CLOCK_FREE : BUFG
--    port map(
--        I => hb_gtwiz_reset_clk_freerun_in,
--        O => hb_gtwiz_reset_clk_freerun_int
--    );

  -- Differential reference clock buffer for mgtrefclk0
  IBUFDS_GTE3_MGTREFCLK0_X0Y3_INST : IBUFDS_GTE3
    generic map (
        REFCLK_EN_TX_PATH  => '0',
        REFCLK_HROW_CK_SEL => "00",
        REFCLK_ICNTL_RX    => "00") 
    port map (
        I     => mgtrefclk0_x0y3_p,
        IB    => mgtrefclk0_x0y3_n,
        CEB   => '0',
        O     => mgtrefclk0_x0y3_int,
        ODIV2 => open
    );

    central_vio : link_vio
    PORT MAP (
        clk            => hb_gtwiz_reset_clk_freerun_int,
        probe_in0(0)   => aligning,
        probe_in1(0)   => aligned,
        probe_in2      => init_done_int,
        probe_in3      => init_retry_ctr_int,
        probe_in4      => gtpowergood_vio_sync,
        probe_in5      => txpmaresetdone_vio_sync,
        probe_in6      => rxpmaresetdone_vio_sync,
        probe_in7      => gtwiz_reset_tx_done_vio_sync,
        probe_in8      => gtwiz_reset_rx_done_vio_sync,
        probe_in9      => sRxbyteisaligned,
        probe_in10     => cdr_stable,
        probe_in11     => data_rd,
        probe_in12     => std_logic_vector(link_delays(0)),
        probe_in13     => std_logic_vector(link_delays(1)),
        probe_in14     => std_logic_vector(link_delays(2)),
        probe_in15     => std_logic_vector(link_delays(3)),
        probe_in16     => std_logic_vector(link_delays(4)),
        probe_in17     => std_logic_vector(link_delays(5)),
        probe_in18     => std_logic_vector(link_delays(6)),
        probe_in19     => std_logic_vector(link_delays(7)),
        probe_in20     => std_logic_vector(measured_delays(0)),
        probe_in21     => std_logic_vector(measured_delays(1)),
        probe_in22     => std_logic_vector(measured_delays(2)),
        probe_in23     => std_logic_vector(measured_delays(3)),
        probe_in24     => std_logic_vector(measured_delays(4)),
        probe_in25     => std_logic_vector(measured_delays(5)),
        probe_in26     => std_logic_vector(measured_delays(6)),
        probe_in27     => std_logic_vector(measured_delays(7)),
        probe_out0     => hb_gtwiz_reset_all_vio_int,
        probe_out1     => hb0_gtwiz_reset_tx_pll_and_datapath_int,
        probe_out2     => hb0_gtwiz_reset_tx_datapath_int,
        probe_out3     => hb_gtwiz_reset_rx_pll_and_datapath_vio_int,
        probe_out4     => hb_gtwiz_reset_rx_datapath_vio_int,
        probe_out5     => link_down_latched_reset_vio_int,
        probe_out6     => loopback,
        probe_out7(0)  => trigger_ila,
        probe_out8(0)  => rst_i2c,
        probe_out9(0)  => str_rd,
        probe_out10(0) => str_wr
    );
    
    fifo_axi_ila : axi_ila_0
    PORT MAP (
        clk => clk,
        trig_in => sCommaDet(0),
        trig_in_ack => open,
        ---- TREADY
        probe0 => (others => '0'), 
        ---- TDATA
        probe1 => rx_data, 
        ---- TSTRB
        probe2(7 downto 0)  => sCommaDet, 
        probe2(31 downto 8) => (others => '0'), 
        ---- TVALID
        probe3 => (others => '0'), 
        ---- TLAST
        probe4 => (others => '0'), 
        ---- TUSER
        probe5 => (others => '0'), 
        ---- TKEEP
        probe6 => (others => '0'), 
        ---- TDEST
        probe7 => (others => '0'),
        ---- TID 
        probe8 => (others => '0') 
    );

    inputs : entity work.inputs
        port map(
            axi_clk     => axi_clk,
            mgtrefclk0  => mgtrefclk0_x0y3_int,
--            mgtrefclk0_p  => mgtrefclk0_x0y3_p,
--            mgtrefclk0_n  => mgtrefclk0_x0y3_n,
                    -- Serial data ports for transceiver channel 0
            ch0_gthrxn_in  => ch0_gthrxn_in,
            ch0_gthrxp_in  => ch0_gthrxp_in,
            ch0_gthtxn_out => ch0_gthtxn_out,
            ch0_gthtxp_out => ch0_gthtxp_out,
          
            -- Serial data ports for transceiver channel 1
            ch1_gthrxn_in  => ch1_gthrxn_in,
            ch1_gthrxp_in  => ch1_gthrxp_in,
            ch1_gthtxn_out => ch1_gthtxn_out,
            ch1_gthtxp_out => ch1_gthtxp_out,
          
            -- Serial data ports for transceiver channel 2
            ch2_gthrxn_in  => ch2_gthrxn_in,
            ch2_gthrxp_in  => ch2_gthrxp_in,
            ch2_gthtxn_out => ch2_gthtxn_out,
            ch2_gthtxp_out => ch2_gthtxp_out,
          
            -- Serial data ports for transceiver channel 3
            ch3_gthrxn_in  => ch3_gthrxn_in,
            ch3_gthrxp_in  => ch3_gthrxp_in,
            ch3_gthtxn_out => ch3_gthtxn_out,
            ch3_gthtxp_out => ch3_gthtxp_out,
          
            -- Serial data ports for transceiver channel 4
            ch4_gthrxn_in  => ch4_gthrxn_in,
            ch4_gthrxp_in  => ch4_gthrxp_in,
            ch4_gthtxn_out => ch4_gthtxn_out,
            ch4_gthtxp_out => ch4_gthtxp_out,
          
            -- Serial data ports for transceiver channel 5
            ch5_gthrxn_in  => ch5_gthrxn_in,
            ch5_gthrxp_in  => ch5_gthrxp_in,
            ch5_gthtxn_out => ch5_gthtxn_out,
            ch5_gthtxp_out => ch5_gthtxp_out,
          
            -- Serial data ports for transceiver channel 6
            ch6_gthrxn_in  => ch6_gthrxn_in,
            ch6_gthrxp_in  => ch6_gthrxp_in,
            ch6_gthtxn_out => ch6_gthtxn_out,
            ch6_gthtxp_out => ch6_gthtxp_out,
          
            -- Serial data ports for transceiver channel 7
            ch7_gthrxn_in  => ch7_gthrxn_in,
            ch7_gthrxp_in  => ch7_gthrxp_in,
            ch7_gthtxn_out => ch7_gthtxn_out,
            ch7_gthtxp_out => ch7_gthtxp_out,
            clk_freerun    => hb_gtwiz_reset_clk_freerun_in,
            rst_freerun    => '1',
            clk_freerun_buf => hb_gtwiz_reset_clk_freerun_int, -- Clock after being globally buffered
--            link_status_out     => link_status_out,
--            link_down_latched_out     => link_down_latched_out,
            init_done_int     => init_done_int,
            init_retry_ctr_int     => init_retry_ctr_int,
            gtpowergood_vio_sync     => gtpowergood_vio_sync,
            txpmaresetdone_vio_sync     => txpmaresetdone_vio_sync,
            rxpmaresetdone_vio_sync     => rxpmaresetdone_vio_sync,
            gtwiz_reset_tx_done_vio_sync     => gtwiz_reset_tx_done_vio_sync,
            gtwiz_reset_rx_done_vio_sync     => gtwiz_reset_rx_done_vio_sync,
            hb_gtwiz_reset_all_vio_int    => hb_gtwiz_reset_all_vio_int,
            hb0_gtwiz_reset_tx_pll_and_datapath_int    => hb0_gtwiz_reset_tx_pll_and_datapath_int,
            hb0_gtwiz_reset_tx_datapath_int    => hb0_gtwiz_reset_tx_datapath_int,
            hb_gtwiz_reset_rx_pll_and_datapath_vio_int    => hb_gtwiz_reset_rx_pll_and_datapath_vio_int,
            hb_gtwiz_reset_rx_datapath_vio_int    => hb_gtwiz_reset_rx_datapath_vio_int,
            link_down_latched_reset_vio_int    => link_down_latched_reset_vio_int,
            loopback    => loopback,
            
            oLinkData => rx_data,

            oEvt_word => evt_word,
            oRxbyteisaligned => sRxbyteisaligned,
            cdr_stable => cdr_stable,
            oBcounter => bcounter,
            oOcounter => ocounter,
            iReady => test_tready,

            clk            => clk,
            rst            => rst,
            q              => d_align,
            oCommaDet      => sCommaDet,
            status_ok      => open,
            status_locked  => open
            );

    align_mon_pre : entity work.align_monitor
        port map (
            clk             => clk,
            rst             => rst,
            d               => d_align,
            bc0             => bc0,
            measured_delays => link_delays,
            aligned         => aligning
        );

    align : entity work.aligner
        port map (
            clk             => clk,
            rst             => rst,
            d               => d_align,
            q               => d_algo
        );

    align_mon : entity work.align_monitor
        port map (
            clk             => clk,
            rst             => rst,
            d               => d_algo,
            bc0             => bc0,
            measured_delays => measured_delays,
            aligned         => aligned
        );

    algo : entity work.algo
        port map(
            clk => clk,
            rst => rst,
            d   => d_algo,
            q   => d_package
            );

    axi_packer : entity work.axi_packager
        port map ( 
                clk     => clk,
                rst     => rst,
                d       => d_package,
                tready  => test_tready,
                rx_data => test_tdata,
                tvalid  => test_tvalid,
                tlast   => test_tlast
               );

--    axi_engine_p : process(axi_clk, axi_rstn, state)
--        variable cnt_last : integer range 0 to 127 := 0;
--    begin
--        if axi_rstn = '0' then
--            fifo_read   <= '0';
--            --        test_tdata <= (others=>'0');
--            test_tvalid <= '0';
--            test_tlast  <= '0';
--            test_tkeep  <= (others => '0');
--            cnt_last    := 0;
--        elsif axi_clk'event and axi_clk = '1' then
--            case state is
--                when idle =>            --test_tdata <= (others=>'0');
--                    test_tvalid <= '0';
--                    test_tkeep  <= (others => '0');
--                    test_tlast  <= '0';
--                    fifo_read   <= '0';
--                    if (unsigned(fifo_cnt) >= unsigned(packet_length)) and m_axis_c2h_tready_0 = '1' then
--                        state <= wait_s1;
--                    else
--                        state <= idle;
--                    end if;
--                when wait_s1 =>         --test_tdata <= (others=>'0');
--                    test_tvalid <= '0';
--                    test_tkeep  <= (others => '0');
--                    test_tlast  <= '0';
--                    fifo_read   <= '1';
--                    state       <= wait_s2;
--                when wait_s2 =>         --test_tdata <= (others=>'0');
--                    test_tvalid <= '0';
--                    test_tkeep  <= (others => '0');
--                    test_tlast  <= '0';
--                    fifo_read   <= '1';
--                    state       <= read;
--                when read =>            --test_tdata <= fifo_dout_s;
--                    test_tvalid <= '1';
--                    test_tkeep  <= (others => '1');
--                    cnt_last    := cnt_last + 1;
--                    if m_axis_c2h_tready_0 = '1' then
--                        if cnt_last < unsigned(packet_length)-1 then
--                            fifo_read <= '1';
--                        else
--                            fifo_read <= '0';
--                        end if;
--                        if cnt_last < unsigned(packet_length) then
--                            test_tlast <= '0';
--                            state      <= read;
--                        else
--                            test_tlast <= '1';
--                            cnt_last   := 0;
--                            state      <= wait_s3;
--                        end if;
--                    else
--                        fifo_read  <= '0';
--                        test_tlast <= '0';
--                        state      <= read;
--                    end if;
--                when wait_s3 =>         --test_tdata <= (others=>'0');
--                    test_tvalid <= '0';
--                    test_tkeep  <= (others => '0');
--                    test_tlast  <= '0';
--                    fifo_read   <= '0';
--                    state       <= wait_s4;
--                when wait_s4 =>         --test_tdata <= (others=>'0');
--                    test_tvalid <= '0';
--                    test_tkeep  <= (others => '0');
--                    test_tlast  <= '0';
--                    fifo_read   <= '0';
--                    state       <= idle;

--                when others => state <= idle;
--            end case;
--        end if;
--    end process;

--    fill_testdata : process(q, m_axis_c2h_tlast_0, m_axis_c2h_tvalid_0)
--    begin
--        for i in 3 + (N_REGION - 1) * 4 downto 0 loop
--            test_tdata((i+1) * 32 - 1 downto i * 32) <= q(i).data;
--            validVec(i)                              <= q(i).valid;
--            doneVec(i)                               <= q(i).done;
--        end loop;
--    end process;
    
    
    fifo : fifo_generator_0
      port map(
        m_aclk => clk,
        s_aclk => axi_clk,
        s_aresetn => axi_rstn,
        s_axis_tvalid => test_tvalid,
        s_axis_tready => test_tready,
        s_axis_tdata => test_tdata,
        s_axis_tkeep => (others => '1'),
        s_axis_tlast => test_tlast,
        m_axis_tvalid => m_axis_c2h_tvalid_0,
        m_axis_tready => m_axis_c2h_tready_0,
        m_axis_tdata => m_axis_c2h_tdata_0,
        m_axis_tkeep => m_axis_c2h_tkeep_0,
        m_axis_tlast => m_axis_c2h_tlast_0
      );
      

    dma_engine_i : entity work.xilinx_dma_pcie_ep
        generic map(
            PL_LINK_CAP_MAX_LINK_WIDTH => 8,
            PL_SIM_FAST_LINK_TRAINING  => false,
            PL_LINK_CAP_MAX_LINK_SPEED => 4,
            C_DATA_WIDTH               => 256,
            EXT_PIPE_SIM               => false,
            C_ROOT_PORT                => false,
            C_DEVICE_NUMBER            => 0)
        port map (
            pci_exp_txp         => pci_exp_txp,
            pci_exp_txn         => pci_exp_txn,
            pci_exp_rxp         => pci_exp_rxp,
            pci_exp_rxn         => pci_exp_rxn,
            sys_clk_p           => sysclk_in_p,
            sys_clk_n           => sysclk_in_n,
            sys_rst_n           => sys_rst_n,
            axi_clk             => axi_clk,  -- This is the clock I want to run the algo with?
            axi_rstn            => axi_rstn,
            pcie_lnk_up         => pcie_lnk_up,
--            m_axis_c2h_tdata_0  => test_tdata,
            m_axis_c2h_tdata_0  => m_axis_c2h_tdata_0,
            m_axis_h2c_tdata_0  => m_axis_h2c_tdata_0,
--            m_axis_c2h_tlast_0  => doneVec(0),
            m_axis_c2h_tlast_0  => m_axis_c2h_tlast_0,
            m_axis_h2c_tlast_0  => m_axis_h2c_tlast_0,
--            m_axis_c2h_tvalid_0 => validVec(0),
            m_axis_c2h_tvalid_0 => m_axis_c2h_tvalid_0,
            m_axis_h2c_tvalid_0 => m_axis_h2c_tvalid_0,
            m_axis_c2h_tready_0 => m_axis_c2h_tready_0,
            m_axis_h2c_tready_0 => m_axis_h2c_tready_0,
            m_axis_c2h_tkeep_0  => m_axis_c2h_tkeep_0,
            m_axis_h2c_tkeep_0  => m_axis_h2c_tkeep_0);

end Behavioral;
