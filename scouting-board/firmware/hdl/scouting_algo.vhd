-- algo
--
-- Algorithm of scouting preprocessor. This also includes the zero suppression.
--
-- D. R. May 2018
library IEEE;
use IEEE.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

use work.datatypes.all;

entity algo is
    port (
        clk : in  std_logic;
        rst : in  std_logic;
        d   : in  ldata;
        q   : out ldata
        );
end algo;

architecture Behavioral of algo is
begin

    -- TODO: Actual algo

    q <= d;

end Behavioral;
