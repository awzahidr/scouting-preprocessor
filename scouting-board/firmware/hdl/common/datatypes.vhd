-- Common data types
--
-- D. R., May 2018
library IEEE;
use IEEE.STD_LOGIC_1164.all;

use IEEE.NUMERIC_STD.ALL;

package datatypes is
    constant LWORD_WIDTH : integer := 32;
    type lword is
    record
        data   : std_logic_vector(LWORD_WIDTH - 1 downto 0);
        valid  : std_logic;
        done   : std_logic;
        strobe : std_logic;
    end record;
    type ldata is array(natural range <>) of lword;
    constant LWORD_NULL : lword             := ((others => '0'), '0', '0', '0');
    constant LDATA_NULL : ldata(0 downto 0) := (0       => LWORD_NULL);

    type TLinkBuffer is array (natural range <>) of ldata(7 downto 0);

    type delay_vector is array (natural range <>) of unsigned(14 downto 0);

    type state_buf is (direct_read, fill_buffer, wait_s1, drain_buffer, wait_s2);

end datatypes;
