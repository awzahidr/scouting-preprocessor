----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/17/2018 04:20:55 PM
-- Design Name: 
-- Module Name: bc0_generator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.top_decl.all;
use work.datatypes.all;

entity bc0_generator is
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           bc0 : out STD_LOGIC);
end bc0_generator;

architecture Behavioral of bc0_generator is

begin

    -- TODO: Allow to delay BC0 here?

    create_orbit : process(clk)
        variable evt_word : natural range 0 to 5;
        variable bcounter : natural range 0 to ORBIT_LENGTH;
    begin
        if clk'event and clk = '1' then
            if evt_word < 5 then
                evt_word := evt_word+1;
                bc0      <= '0';
            elsif evt_word = 5 then
                evt_word := 0;
                if bcounter < ORBIT_LENGTH then
                    bcounter := bcounter+1;
                    bc0      <= '0';
                elsif bcounter = ORBIT_LENGTH then
                    bcounter := 1;
                    bc0      <= '1';
                else
                    bcounter := 1;
                    bc0      <= '1';
                end if;
            end if;
        end if;
    end process;

end Behavioral;
