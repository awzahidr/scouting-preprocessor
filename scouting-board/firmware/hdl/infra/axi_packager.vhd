----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/14/2018 05:49:01 PM
-- Design Name: 
-- Module Name: axi_packager - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.top_decl.all;
use work.datatypes.all;

entity axi_packager is
    Port ( clk     : in STD_LOGIC;
           rst     : in STD_LOGIC;
           d       : in ldata;
           tready  : in std_logic;
           rx_data : out std_logic_vector(255 downto 0);
           tvalid  : out std_logic;
           tlast   : out std_logic;
           state_out : out state_buf;
           word_cnt : out unsigned(31 downto 0);
           orbit_st : out std_logic;
           rd_point : out unsigned(3 downto 0);
           d_o      : out ldata(7 downto 0);
           d_buf    : out TLinkBuffer(6 downto 0)
           );
end axi_packager;

architecture Behavioral of axi_packager is
    signal d_out     : ldata(7 downto 0);
    signal d_delayed : ldata(7 downto 0);

    signal delayed_valid : std_logic;
    signal orbit_start   : std_logic;

    signal d_buffer : TLinkBuffer(6 downto 0); -- TODO: We might not need the extra slot, check that!
    signal rd_ptr   : unsigned(3 downto 0) := to_unsigned(0, 4);
    
    signal buffer_valid : std_logic;

begin

    -- DEBUG
    orbit_st <= orbit_start;
    rd_point <= rd_ptr;
    d_o <= d_out;
    d_buf <= d_buffer;


    -- TODO: Make package of fixed length. (Not necessarily as long as a packet)
    create_packet : process(clk)
    begin
        if clk'event and clk = '1' then -- Make sure we react properly to ready signal.
            d_delayed <= d;
        end if;
    end process;
    d_delayed(0).done <= d_delayed(0).valid and not d(0).valid;

    d_buffer(0) <= d_delayed;

    delay_valid : process(clk)
    begin
        if clk'event and clk = '1' then
            delayed_valid <= d(0).valid;  -- Data is supposed to be aligned here.
        end if;
    end process;
    orbit_start <= d(0).valid and not delayed_valid;
    
    buffer_data : process(clk)
        variable state        : state_buf := direct_read;
        variable word_counter : natural range 0 to 5;
        variable buf_valid    : std_logic := '1';
--        variable complete_evt : std_logic;
    begin
        if clk'event and clk = '1' then
            if rst = '1' then
                word_counter := 0;
                rd_ptr       <= to_unsigned(0, 4);
                state        := direct_read;
                buf_valid    := '1';
            else
                if orbit_start = '1' then
                    word_counter := 0;  -- To check: Might need to make this 1.. 
                elsif d(0).valid = '1' then
                    if word_counter < 5 then
                        word_counter := word_counter + 1;
                    else
                        word_counter := 0;
                    end if;
                end if;

                -- TODO: When data is not valid we can discard it and not move the pointer.
                case state is
                    when direct_read =>
                        if tready = '0' and d(0).valid = '1' then
--                            rd_ptr                           <= rd_ptr + to_unsigned(1, 4);
--                            d_buffer(d_buffer'high downto 1) <= d_buffer(d_buffer'high-1 downto 0);
                            state := fill_buffer;
                        else
                            rd_ptr <= to_unsigned(0, 4);
                        end if;
                    when fill_buffer =>
                        -- TODO: What happens if we return to ready before buffer is full?
                        if word_counter = 0 then
                            -- TODO: If we're ready here already we can go directly to drain. (otherwise we might hold data for one clk too long)
                            state                            := wait_s1;
                            rd_ptr                           <= rd_ptr + to_unsigned(1, 4);
                            d_buffer(d_buffer'high downto 1) <= d_buffer(d_buffer'high-1 downto 0); -- Shift in one last time to protect the last word from being overwritten
                        elsif d(0).valid = '0' then
                            state := fill_buffer;
                        else
                            rd_ptr <= rd_ptr + to_unsigned(1, 4);
                            -- Shift in the data
                            d_buffer(d_buffer'high downto 1) <= d_buffer(d_buffer'high-1 downto 0);
                        end if;
                    when wait_s1 =>
                        -- Do nothing
                        if tready = '1' then
                            state := drain_buffer;
                        end if;
                    when drain_buffer =>
                        if rd_ptr > 1 then
                            rd_ptr <= to_unsigned(to_integer(rd_ptr) - 1, 4);

                        else
                            if word_counter = 5 then
                                buf_valid := '1';
                                rd_ptr <= to_unsigned(to_integer(rd_ptr) - 1, 4);
                                state := direct_read;
                            else
                                buf_valid := '0'; -- TODO: Make buf_valid a variable?
                                rd_ptr <= to_unsigned(to_integer(rd_ptr) - 1, 4);
                                state := wait_s2;
                            end if;
                        end if;
                    when wait_s2 =>
                        if word_counter = 0 then
                            buf_valid := '1';
                            state     := direct_read;
                        end if;
                    when others =>
                        -- Wait until we get a start word again.
                        if word_counter = 5 then
                            buf_valid := '1';
                            state     := direct_read;
                        else
                            buf_valid := '0';
                            state     := wait_s2;
                        end if;
                end case;

            end if;
            
            -- DEBUG
            word_cnt <= to_unsigned(word_counter, 32);
            state_out <= state;

        end if;

        buffer_valid <= buf_valid;
--        -- TODO: This here is one clk late wrt the tready signal!
--        if buf_valid = '1' then
--            d_out <= d_buffer(to_integer(rd_ptr));
--        else
--            d_out <= (others => LWORD_NULL);
--        end if;

    end process;

    assign_data: process(d_out, tready, buffer_valid, d_buffer)
    begin
        if buffer_valid = '1' then
            d_out <= d_buffer(to_integer(rd_ptr));
        else
            d_out <= (others => LWORD_NULL);
        end if;

        if tready = '1' then
            for i in d_delayed'range loop
                rx_data(i*32 + 31 downto i*32) <= d_out(i).data;
            end loop;
            tvalid <= d_out(0).valid;
            tlast  <= d_out(0).done;
        end if;
    end process;

end Behavioral;
