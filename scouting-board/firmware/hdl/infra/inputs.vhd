-- inputs
--
-- Optical serial inputs to 40 MHz scouting demonstrator
--
-- D. R. May 2018
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.top_decl.all;
use work.datatypes.all;

entity inputs is
    port (
        axi_clk : in std_logic;
    
        -- Serial data ports for transceiver channel 0
        ch0_gthrxn_in  : in std_logic;
        ch0_gthrxp_in  : in std_logic;
        ch0_gthtxn_out : out std_logic;
        ch0_gthtxp_out : out std_logic;
        
        -- Serial data ports for transceiver channel 1
        ch1_gthrxn_in  : in std_logic;
        ch1_gthrxp_in  : in std_logic;
        ch1_gthtxn_out : out std_logic;
        ch1_gthtxp_out : out std_logic;
        
        -- Serial data ports for transceiver channel 2
        ch2_gthrxn_in  : in std_logic;
        ch2_gthrxp_in  : in std_logic;
        ch2_gthtxn_out : out std_logic;
        ch2_gthtxp_out : out std_logic;
        
        -- Serial data ports for transceiver channel 3
        ch3_gthrxn_in  : in std_logic;
        ch3_gthrxp_in  : in std_logic;
        ch3_gthtxn_out : out std_logic;
        ch3_gthtxp_out : out std_logic;
        
        -- Serial data ports for transceiver channel 4
        ch4_gthrxn_in  : in std_logic;
        ch4_gthrxp_in  : in std_logic;
        ch4_gthtxn_out : out std_logic;
        ch4_gthtxp_out : out std_logic;
        
        -- Serial data ports for transceiver channel 5
        ch5_gthrxn_in  : in std_logic;
        ch5_gthrxp_in  : in std_logic;
        ch5_gthtxn_out : out std_logic;
        ch5_gthtxp_out : out std_logic;
        
        -- Serial data ports for transceiver channel 6
        ch6_gthrxn_in  : in std_logic;
        ch6_gthrxp_in  : in std_logic;
        ch6_gthtxn_out : out std_logic;
        ch6_gthtxp_out : out std_logic;
        
        -- Serial data ports for transceiver channel 7
        ch7_gthrxn_in  : in  std_logic;
        ch7_gthrxp_in  : in  std_logic;
        ch7_gthtxn_out : out std_logic;
        ch7_gthtxp_out : out std_logic;
        mgtrefclk0     : in  std_logic;
--        mgtrefclk0_p   : in  std_logic;
--        mgtrefclk0_n   : in  std_logic;
        clk_freerun    : in  std_logic;
        rst_freerun    : in  std_logic;
        clk_freerun_buf : out std_logic;
--        link_status_out                            : out std_logic_vector(0 downto 0);
--        link_down_latched_out                      : out std_logic_vector(0 downto 0);
        init_done_int                              : out std_logic_vector(0 downto 0);
        init_retry_ctr_int                         : out std_logic_vector(3 downto 0);
        gtpowergood_vio_sync                       : out std_logic_vector(7 downto 0);
        txpmaresetdone_vio_sync                    : out std_logic_vector(7 downto 0);
        rxpmaresetdone_vio_sync                    : out std_logic_vector(7 downto 0);
        gtwiz_reset_tx_done_vio_sync               : out std_logic_vector(0 downto 0);
        gtwiz_reset_rx_done_vio_sync               : out std_logic_vector(0 downto 0);
        hb_gtwiz_reset_all_vio_int                 : in  std_logic_vector(0 downto 0);
        hb0_gtwiz_reset_tx_pll_and_datapath_int    : in  std_logic_vector(0 downto 0);
        hb0_gtwiz_reset_tx_datapath_int            : in  std_logic_vector(0 downto 0);
        hb_gtwiz_reset_rx_pll_and_datapath_vio_int : in  std_logic_vector(0 downto 0);
        hb_gtwiz_reset_rx_datapath_vio_int         : in  std_logic_vector(0 downto 0);
        link_down_latched_reset_vio_int            : in  std_logic_vector(0 downto 0);
        loopback                                   : in  std_logic_vector(23 downto 0);
        clk            : out std_logic;
        rst            : out std_logic;
        q              : out ldata;
        oCommaDet      : out std_logic_vector(7 downto 0);
        status_ok      : out std_logic;
        status_locked  : out std_logic;

        oLinkData : out std_logic_vector(8 * 32 - 1 downto 0);
        
        cdr_stable : out std_logic_vector(0 downto 0);
        
        oOcounter : out std_logic_vector(31 downto 0);
        oBcounter : out std_logic_vector(31 downto 0);
        oEvt_word : out std_logic_vector(31 downto 0);
        oRxbyteisaligned : out std_logic_vector(7 downto 0);
        iReady    : in std_logic
        );
end inputs;

architecture Behavioral of inputs is

    constant BYPASS_LINKS : boolean := false;

    signal align_marker   : std_logic;
    signal sStatus_ok     : std_logic_vector(N_REGION - 1 downto 0);
    signal sStatus_locked : std_logic_vector(N_REGION - 1 downto 0);
    
    signal rx_data : std_logic_vector(8 * 32 - 1 downto 0);
    signal tx_data : std_logic_vector(8 * 32 - 1 downto 0);
    signal usr_clk : std_logic;
    
    signal valid_delayed : std_logic;
    
    signal ocounter : natural := 0;
    signal bcounter : natural := 0;
    signal evt_word : natural range 0 to 5 := 0;
    
    signal sCommaDet : std_logic_vector(7 downto 0);
    signal q_int     : ldata(7 downto 0);
    signal sCharisk  : std_logic_vector(63 downto 0);


begin
    -- TODO: Generate alignment signal ("BC0") here and distribute to regions.

--    rgen : for i in 0 to N_REGION - 1 generate
--        region : entity work.region
--            -- generic map(
--            --     INDEX => 1
--            --     )
--            port map(
--                clk           => clk,
--                rst           => rst,
--                align_marker  => align_marker,
--                q             => q(3 + 4*i downto 4*i),
--                status_ok     => sStatus_ok(i),
--                status_locked => sStatus_locked(i)
--                );
--    end generate;
    
    gth_wrapper : entity work.gtwizard_ultrascale_0_example_top
        port map
        (
        -- Differential reference clock inputs
        mgtrefclk0 => mgtrefclk0,
--        mgtrefclk0_x0y3_p => mgtrefclk0_p,
--        mgtrefclk0_x0y3_n => mgtrefclk0_p,
      
        -- Serial data ports for transceiver channel 0
        ch0_gthrxn_in  => ch0_gthrxn_in,
        ch0_gthrxp_in  => ch0_gthrxp_in,
        ch0_gthtxn_out => ch0_gthtxn_out,
        ch0_gthtxp_out => ch0_gthtxp_out,
      
        -- Serial data ports for transceiver channel 1
        ch1_gthrxn_in  => ch1_gthrxn_in,
        ch1_gthrxp_in  => ch1_gthrxp_in,
        ch1_gthtxn_out => ch1_gthtxn_out,
        ch1_gthtxp_out => ch1_gthtxp_out,
      
        -- Serial data ports for transceiver channel 2
        ch2_gthrxn_in  => ch2_gthrxn_in,
        ch2_gthrxp_in  => ch2_gthrxp_in,
        ch2_gthtxn_out => ch2_gthtxn_out,
        ch2_gthtxp_out => ch2_gthtxp_out,
      
        -- Serial data ports for transceiver channel 3
        ch3_gthrxn_in  => ch3_gthrxn_in,
        ch3_gthrxp_in  => ch3_gthrxp_in,
        ch3_gthtxn_out => ch3_gthtxn_out,
        ch3_gthtxp_out => ch3_gthtxp_out,
      
        -- Serial data ports for transceiver channel 4
        ch4_gthrxn_in  => ch4_gthrxn_in,
        ch4_gthrxp_in  => ch4_gthrxp_in,
        ch4_gthtxn_out => ch4_gthtxn_out,
        ch4_gthtxp_out => ch4_gthtxp_out,
      
        -- Serial data ports for transceiver channel 5
        ch5_gthrxn_in  => ch5_gthrxn_in,
        ch5_gthrxp_in  => ch5_gthrxp_in,
        ch5_gthtxn_out => ch5_gthtxn_out,
        ch5_gthtxp_out => ch5_gthtxp_out,
      
        -- Serial data ports for transceiver channel 6
        ch6_gthrxn_in  => ch6_gthrxn_in,
        ch6_gthrxp_in  => ch6_gthrxp_in,
        ch6_gthtxn_out => ch6_gthtxn_out,
        ch6_gthtxp_out => ch6_gthtxp_out,
      
        -- Serial data ports for transceiver channel 7
        ch7_gthrxn_in  => ch7_gthrxn_in,
        ch7_gthrxp_in  => ch7_gthrxp_in,
        ch7_gthtxn_out => ch7_gthtxn_out,
        ch7_gthtxp_out => ch7_gthtxp_out,
      
        -- User-provided ports for reset helper block(s)
        hb_gtwiz_reset_clk_freerun_in => clk_freerun,
        hb_gtwiz_reset_all_in => rst_freerun,
        hb_gtwiz_reset_clk_freerun_out => clk_freerun_buf,
        
--        link_status_out     => link_status_out(0),
--        link_down_latched_out     => link_down_latched_out(0),
        init_done_int     => init_done_int(0),
        init_retry_ctr_int     => init_retry_ctr_int,
        gtpowergood_vio_sync     => gtpowergood_vio_sync,
        txpmaresetdone_vio_sync     => txpmaresetdone_vio_sync,
        rxpmaresetdone_vio_sync     => rxpmaresetdone_vio_sync,
        gtwiz_reset_tx_done_vio_sync     => gtwiz_reset_tx_done_vio_sync(0),
        gtwiz_reset_rx_done_vio_sync     => gtwiz_reset_rx_done_vio_sync(0),
        hb_gtwiz_reset_all_vio_int    => hb_gtwiz_reset_all_vio_int,
        hb0_gtwiz_reset_tx_pll_and_datapath_int    => hb0_gtwiz_reset_tx_pll_and_datapath_int,
        hb0_gtwiz_reset_tx_datapath_int    => hb0_gtwiz_reset_tx_datapath_int,
        hb_gtwiz_reset_rx_pll_and_datapath_vio_int    => hb_gtwiz_reset_rx_pll_and_datapath_vio_int,
        hb_gtwiz_reset_rx_datapath_vio_int    => hb_gtwiz_reset_rx_datapath_vio_int,
        link_down_latched_reset_vio_int    => link_down_latched_reset_vio_int,
        loopback    => loopback,
        rxbyteisaligned_out => oRxbyteisaligned,
      
        -- PRBS-based link status ports
--        link_down_latched_reset_in => missing,
        
        rxcommadet_out => sCommaDet,
        gtwiz_userdata_rx_out => rx_data,
        gtwiz_userdata_tx_in => tx_data,
        charisk_in => sCharisk,
        gtwiz_userclk_rx_srcclk_out => usr_clk,
        gtwiz_reset_rx_cdr_stable_out => cdr_stable
    );

    clk       <= usr_clk;
    oCommaDet <= sCommaDet;


    data_assignment : for i in 0 to 7 generate
        bypass : if BYPASS_LINKS = true generate
            q(i) <= q_int(i);
        end generate bypass;
        loopback : if BYPASS_LINKS = false generate
            q(i).data <= rx_data(i*32 + 31 downto i*32);
            q(i).valid <= not sCommaDet(i);
        end generate loopback;
    end generate;
    oLinkData <= rx_data;

    
    create_testdata : process(usr_clk)
    begin
        if usr_clk'event and usr_clk = '1' then
            if evt_word < 5 then
                evt_word <= evt_word+1;
            elsif evt_word = 5 then
                evt_word <= 0;
                if bcounter < ORBIT_LENGTH then
                    bcounter <= bcounter+1;
                elsif bcounter = ORBIT_LENGTH then
                    bcounter <= 1;
                    ocounter <= ocounter+1;
                else
                    bcounter <= 1;
                end if;
            else
                evt_word <= 0;
            end if;

            -- Create test data now
            for i in 0 to 7 loop         -- Number of channels
                if bcounter < ORBIT_LENGTH-3 then -- Arbitrary obit gap
                    if evt_word = 0 then
                        tx_data(i*32 + 31 downto i*32)  <= std_logic_vector(to_unsigned(bcounter, 32));
                        q_int(i).data  <= std_logic_vector(to_unsigned(bcounter, 32));
                    elsif evt_word = 1 then
                        tx_data(i*32 + 31 downto i*32)  <= std_logic_vector(to_unsigned(ocounter, 32));
                        q_int(i).data  <= std_logic_vector(to_unsigned(ocounter, 32));
                    elsif (evt_word = 2) or (evt_word = 4) then
                        tx_data(i*32 + 31 downto i*32)  <= "000000001" & "1100" & "100000000" & "0000000000";
                        q_int(i).data  <= "000000001" & "1100" & "100000000" & "0000000000";
                    elsif (evt_word = 3) or (evt_word = 5) then
                        tx_data(i*32 + 31 downto i*32)  <= (others => '1');
--                        tx_data(i*32 + 31 downto i*32)  <= "00000000000" & "0000000001" & "0111111" & "10" & "00";
                        q_int(i).data  <= (others => '1');
--                        q_int(i).data  <= "00000000000" & "0000000001" & "0111111" & "10" & "00";
                    else
                        tx_data(i*32 + 31 downto i*32)  <= (others => '1');
                        q_int(i).data  <= (others => '1');
                    end if;

                    q_int(i).valid <= '1';
                    sCharisk(i*8 + 7 downto i*8) <= "00000000";
                    if bcounter = ORBIT_LENGTH-4 and evt_word = 5 then
                        q_int(i).done  <= '1';
                    else
                        q_int(i).done  <= '0';
                    end if;
                else
                    q_int(i).done  <= '0';
                    q_int(i).valid <= '0';
                    q_int(i).data  <= (others => '1'); -- If I see a succession of all ones I know I'm transmitting non-valid data.
                    tx_data(i*32 + 31 downto i*32) <= "10111100101111001011110010111100"; -- COMMA
                    sCharisk(i*8 + 7 downto i*8) <= "00001111";

                end if;
            end loop;

        end if;
    end process;

    oEvt_word <= std_logic_vector(to_unsigned(evt_word, 32));
    oOcounter <= std_logic_vector(to_unsigned(ocounter, 32));
    oBcounter <= std_logic_vector(to_unsigned(bcounter, 32));

    -- TODO: OR the status signals here.

end Behavioral;
