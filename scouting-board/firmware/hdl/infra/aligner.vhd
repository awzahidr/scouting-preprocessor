----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Hannes Sakulin
-- 
-- Create Date: 09/18/2018 09:38:36 AM
-- Design Name: 
-- Module Name: aligner - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use work.top_decl.all;
use work.datatypes.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

 
entity aligner is
    Generic (
           BUFFER_SIZE : integer := 64;
           NSTREAMS : integer := 8
           );
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC; -- active high pulse
           start_align : in STD_LOGIC := '1'; -- active high pulse
           d : in ldata(NSTREAMS-1 downto 0);
           q : out ldata(NSTREAMS-1 downto 0));
end aligner;

architecture Behavioral of aligner is

    signal all_at_edge : STD_LOGIC;
    signal stream_at_edge : STD_LOGIC_VECTOR(NSTREAMS -1 downto 0);
    signal aligning : std_logic;
begin

    stream_aligner: for i in NSTREAMS - 1 downto 0 generate
        signal write_counter : std_logic_vector(16 downto 0);
        signal read_counter : std_logic_vector(16 downto 0);
        signal buf : ldata(0 to BUFFER_SIZE -1); 
        signal last_dv : lword;
        signal enable_read_counter : STD_LOGIC;
    begin
    
        bla: process (clk) is
        begin
        if clk'event and clk='1' then
            if rst='1' then
                -- start write counter at 0 and read counter at end of buffer
                write_counter <= (others => '0');
                read_counter <= std_logic_vector(to_unsigned(BUFFER_SIZE-1, read_counter'length));
            else
              buf( TO_INTEGER(UNSIGNED(write_counter)) ) <= d(i);

              -- inc write counter 
              write_counter <= std_logic_vector( to_unsigned( (TO_INTEGER(UNSIGNED(write_counter)) +1 ) mod BUFFER_SIZE, write_counter'length));

              if enable_read_counter='1' then              
                 last_dv <= buf( TO_INTEGER(UNSIGNED(read_counter)) );            
                 -- inc read counter
                 read_counter <= std_logic_vector( to_unsigned( (TO_INTEGER(UNSIGNED(read_counter)) +1 ) mod BUFFER_SIZE, write_counter'length));
              end if;
            end if;
        end if;
        end process bla;
        
        stream_at_edge(i) <= (NOT last_dv.valid ) AND  buf( TO_INTEGER(UNSIGNED(read_counter)) ).valid;
        -- block read counter when we are aligning until all edges are seen
        enable_read_counter <= ( NOT aligning) OR 
                               ( aligning AND ( all_at_edge OR ( NOT stream_at_edge(i) )));
        q(i) <= last_dv;
        
    end generate stream_aligner;

    -- is there a better way to formulate this asynchronously?
    all_at_edge <= '1' when (TO_INTEGER(UNSIGNED(not stream_at_edge)) = 0) else '0';
    
    global_align_flag: process (clk) is
    begin
        if clk'event and clk='1' then
            if rst='1' then 
                aligning <= '0';
            else     
                if all_at_edge = '1' then
                    aligning <= '0';
                elsif start_align = '1' then
                    aligning <= '1';
                end if;
            end if;
        end if;
    end process global_align_flag;

end Behavioral;